# Python 리스트 메소드: Append, Pop, Sort <sup>[1](#footnote_1)</sup>

> <font size="3">요소를 추가, 제거 및 정렬하기 위해 리스트 메소드를 사용하는 방법에 대해 알아본다.</font>

## 목차
1. [소개](./list-methods.md#intro)
1. [Python에서 리스트란?](./list-methods.md#sec_02)
1. [리스트 생성과 액세스 방법](./list-methods.md#sec_03)
1. [Append로 리스트에 요소 추가 방법](./list-methods.md#sec_04)
1. [Pop으로 리스트에서 요소 제거 방법](./list-methods.md#sec_05)
1. [Sort로 리스트 정렬 방법](./list-methods.md#sec_06)
1. [맺으며](./list-methods.md#conclusion)

<a name="footnote_1">1</a>: [Python Tutorial 9 — Python List Methods: Append, Pop, Sort](https://python.plainenglish.io/python-tutorial-9-python-list-methods-append-pop-sort-570ed92ee8d4?sk=3e50117746c347cff11043c0c892afb4)를 편역한 것이다.
