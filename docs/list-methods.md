# Python 리스트 메소드: Append, Pop, Sort

## <a name="intro"></a> 소개
이 포스팅에서는 Python에서 가장 일반적이고 유용한 리스트 메소드를 사용하는 방법을 설명할 것이다. 리스트는 Python에서 가장 다재다능하고 자주 사용되는 데이터 구조 중 하나로, 어떤 데이터 타입의 요소든 하나의 변수에 얼마든지 저장할 수 있다. 리스트 메소드은 리그트 객체에 적용하여 요소를 추가, 제거, 정렬 등의 다양한 작업을 수행할 수 있는 내장 함수이다.

이 글이 끝날 때까지 다음 작업을 수행할 수 있다.

- Python에서 리스트 작성과 액세스
- append 메소드을 사용하여 리스트에 요소 추가
- pop 메소드를 사용하여 리스트에서 요소 제거
- sort 메소드를 사용하여 리스트 정렬

시작하기 전에 Python 구문과 데이터 타입에 대한 기본 지식을 갖추고 있다고 가정한다. 또한 온라인 파이썬 인터프리터나 원하는 코드 편집기를 사용하여 코드 예와 함께 따라할 수 있다.

## <a name="sec_02"></a> Python에서 리스트란?
Python의 리스트에는 숫자, 문자열, 불리언 또는 기타 리스트 등 다양한 데이터 타입의 요소 컬렉션이다. 리스트는 순서가 있고 변경 가능한 데이터 구조이다. 이는 인데스를 사용하여 리스트의 요소를 수정과 삭제할 수 있음을 의미한다. 리스트는 반복 가능(iterable)하다. 즉 for 루프 또는 while 루프를 이용하여 그 요소를 루프에 사용할 수 있다는 것을 의미한다.

Python에서 리스트를 만들려면 대괄호 `[]`와 쉼표(`,`)로 구분할 수 있습니다. 예를 들어, 다음 코드는 수 두 개, 한 문자열 및 한 부울의 4개 요소로 이루어진 리스트를 생성한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
```

리스트의 요소에 접근하기 위해서는 첫 번째 요소인 0에서 시작하여 마지막 요소의 경우 리스트 길이에서 1을 뺀 값까지 올라가는 인덱스 위치를 사용할 수 있다. 또한 음수 인덱스를 사용하여 리스트의 끝인 마지막 요소의 경우 -1부터 시작하여 첫 번째 요소의 경우 -길이까지 액세스할 수 있다. 예를 들어 다음 코드는 양수와 음수 인덱스를 사용하여 리스트의 첫 번째 요소와 마지막 요소를 액세스한다.

```python
# Access the first element of the list
first_element = my_list[0]

# Print the first element
print(first_element)

# Access the last element of the list
last_element = my_list[-1]

# Print the last element
print(last_element)
```

이 코드의 출력은 다음과 같다.

```
42
True
```

다음 섹션에서는 리스트를 만들고 액세스하는 방법에 대해 자세히 알아본다.

## <a name="sec_03"></a> 리스트 생성과 액세스 방법
이 섹션에서는 Python으로 리스트를 작성하고 접근하는 방법을 설명할 것이다. 앞 색션에서 언급했듯이 리스트는 숫자, 문자열, 부울, 또는 다른 리스트와 같이 다양한 데이터 타입의 요소들 컬렉션이다. 리스크는 순서가 지정되고 변경 가능한 데이터 구조이며 인덱스 위치를 사용하여 해당 요소에 접근하고 수정과 삭제할 수 있다. 또한 리스트는 반복할 수 있으므로 for 루프 또는 while 루프를 사용하여 해당 요소를 순환할 수 있다.

Python에서 리스트를 만들려면 대괄호 `[ ]`를 사용하고 요소들을 쉼표(`,`)로 구분하면 된다. 예를 들어 다음 코드는 숫자 2개, 문자열 1개, 부울 1개의 네 가지 요소의 리스트를 만든다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
```

list() 함수를 사용하여 리스트를 만들 수도 있다. list() 함수는 반복 가능한 객체를 인수로 받아 그 요소들의 리스트를 반환한다. 예를 들어, 다음 코드는 문자열 `"Python"`의 문자 리스트를 만든다.

```python
# Create a list using the list() function
my_list = list("Python")

# Print the list
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
['P', 'y', 't', 'h', 'o', 'n']
```

리스트의 요소에 접근하기 위해서는 첫 번째 요소의 경우 0에서 마지막 요소의 경우 1을 뺀 값으로 리스트의 길이까지 올라가는 인덱스 위치를 사용할 수 있다. 또한 음수 인덱스를 사용하여 리스트의 끝인 마지막 요소의 경우 -1부터 시작하여 첫 번째 요소의 경우 -길이까지 액세스할 수 있다. 예를 들어 다음 코드는 양수와 음수 인덱스를 사용하여 리스트의 첫 번째 요소와 마지막 요소에 접근한다.

```python
# Access the first element of the list
first_element = my_list[0]

# Print the first element
print(first_element)

# Access the last element of the list
last_element = my_list[-1]

# Print the last element
print(last_element)
```

이 코드의 출력은 다음과 같다.

```
P
n
```

콜론(`:`)으로 구분된 start 인덱스와 end 인덱스를 지정하여 리스트에서 다양한 요소에 액세스할 수도 있다. start 인덱스의 요소는 포함되고 end 인덱스의 요소는 제외된다. 예를 들어 다음 코드는 리스트의 인덱스 1부터 3까지의 요소를 액세스한다.

```python
# Access a range of elements from the list
slice = my_list[1:4]

# Print the slice
print(slice)
```

이 코드의 출력은 다음과 같다.

```
['y', 't', 'h']
```

start 인덱스를 생략하면 기본값은 0이고, end 인덱스를 생략하면 기본값은 리스트의 길이이다. 예를 들어, 다음 코드는 리스트의 처음부터 인덱스 2까지, 인덱스 3부터 끝까지 요소에 접근한다.

```python
# Access the elements from the beginning to index 2
slice1 = my_list[:3]

# Print the slice
print(slice1)

# Access the elements from index 3 to the end of the list
slice2 = my_list[3:]

# Print the slice
print(slice2)
```

이 코드의 출력은 다음과 같다.

```
['P', 'y', 't']
['h', 'o', 'n']
```

다음 절에서는 append 메소드를 사용하여 리스트에 요소를 추가하는 방법을 다룰 것이다.

## <a name="sec_04"></a> Append로 리스트에 요소 추가 방법
이 절에서는 append 메소드를 사용하여 리스트에 요소를 추가하는 방법을 설명할 것이다. append 메소드는 하나의 인수를 취하여 리스트의 끝에 추가하는 내장 함수이다. append 메소드는 원래의 리스트를 수정하고 새로운 리스트를 반환하지 않는다. 예를 들어, 다음 코드는 리스트의 끝에 `"World"`라는 문자열을 추가한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list before appending
print(my_list)

# Add the string "World" to the end of the list using the append method
my_list.append("World")

# Print the list after appending
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
[42, 3.14, 'Hello', True, 'World']
```

append 메소드를 사용하여 리스트의 끝에 다른 리스트를 추가할 수도 있다. 그러나 이렇게 하면 리스트 내의 리스트인 중첩된 리스트이 생성된다. 예를 들어 다음 코드는 리스트의 끝에 리스트 [1, 2, 3]를 추가한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list before appending
print(my_list)

# Add the list [1, 2, 3] to the end of the list using the append method
my_list.append([1, 2, 3])

# Print the list after appending
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
[42, 3.14, 'Hello', True, [1, 2, 3]]
```

중첩된 리스트를 만들지 않고 리스트의 끝에 다른 리스트의 요소를 추가하려면 append 메소드 대신 extend 메소드를 사용하여야 한다. extend 메소드는 반복 가능한 객체를 인수로 가져와 리스트의 끝에 해당 객체의 요소를 추가한다. extend 메소드는 원래 리스트를 수정하고 새로운 리스트는 반환하지 않는다. 예를 들어 다음 코드는 extend 메소드를 사용하여 리스트 [1, 2, 3] 요소를 리스트의 끝에 추가한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list before extending
print(my_list)

# Add the elements of the list [1, 2, 3] to the end of the list using the extend method
my_list.extend([1, 2, 3])

# Print the list after extending
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
[42, 3.14, 'Hello', True, 1, 2, 3]
```

다음 절에서는 pop 메소드를 사용하여 리스트에서 요소를 제거하는 방법을 설명할 것이다.

## <a name="sec_05"></a> Pop으로 리스트에서 요소 제거 방법
이 절에서는 pop 메소드를 사용하여 리스트에서 요소를 제거하는 방법을 설명할 것이다. pop 메소드는 옵션 인수를 취하고 주어진 인덱스 위치의 요소를 제거하고 반환하는 내장 함수이다. 만약 인수가 주어지지 않으면 pop 메소드는 리스트의 마지막 요소를 제거하고 반환한다. pop 메소드는 원래의 리스트을 수정하고 새로운 리스트을 반환하지 않는다. 예를 들어 다음 코드는 pop 메소드를 사용하여 리스트의 마지막 요소를 제거하고 반환한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list before popping
print(my_list)

# Remove and return the last element of the list using the pop method
popped_element = my_list.pop()

# Print the popped element
print(popped_element)

# Print the list after popping
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
True
[42, 3.14, 'Hello']
```

pop 메소드를 사용하여 제거하고 반환하고자 하는 요소의 인덱스 위치를 지정할 수도 있다. 예를 들어, 다음 코드는 pop 메소드를 사용하여 리스트 `my_list`의 인덱스 1에 있는 요소를 제거하고 반환한다.

```python
# Create a list of four elements
my_list = [42, 3.14, "Hello", True]

# Print the list before popping
print(my_list)

# Remove and return the element at index 1 of the list using the pop method
popped_element = my_list.pop(1)

# Print the popped element
print(popped_element)

# Print the list after popping
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[42, 3.14, 'Hello', True]
3.14
[42, 'Hello', True]
```

빈 리스트나 존재하지 않는 인덱스 위치에서 요소를 pop하려고 하면 `IndexError`가 나온다. 예를 들어 다음 코드는 빈 리스트와 범위를 벗어난 인덱스 위치에서 요소를 pop하려고 한다.

```python
# Create an empty list
my_list = []

# Try to pop an element from an empty list
my_list.pop()

# Try to pop an element from an index position that does not exist
my_list.pop(5)
```

이 코드의 출력은 다음과 같다.

```
Traceback (most recent call last):
    File "", line 5, in 
IndexError: pop from empty list

Traceback (most recent call last):
    File "", line 8, in 
IndexError: pop index out of range
```

다음 절에서는 sort 메소드를 사용하여 리스트을 정렬하는 방법을 설명할 것이다

## <a name="sec_06"></a> Sort로 리스트 정렬 방법
이 절에서는 sort 메소드를 사용하여 리스트을 정렬하는 방법을 설명할 것이다. sort 메소드는 리스트의 요소를 오름차순 또는 내림차순으로 정렬하는 내장 함수이다. sort 메소드는 원래의 리스트을 수정하고 새로운 리스트를 반환하지 않는다. 예를 들어 다음 코드는 sort 메소드를 사용하여 수의 리스트를 오름차순으로 정렬한다.

```python
# Create a list of numbers
my_list = [5, 2, 7, 9, 1, 4, 6, 8, 3]

# Print the list before sorting
print(my_list)

# Sort the list in ascending order using the sort method
my_list.sort()

# Print the list after sorting
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[5, 2, 7, 9, 1, 4, 6, 8, 3]
[1, 2, 3, 4, 5, 6, 7, 8, 9]
```

sort 메소드에 `reverse=True` 인수를 전달하여 리스트를 내림차순으로 정렬할 수도 있다. 예를 들어, 다음 코드는 sort 메소드를 사용하여 리스트을 내림차순으로 정렬한다.

```python
# Create a list of numbers
    my_list = [5, 2, 7, 9, 1, 4, 6, 8, 3]

    # Print the list before sorting
    print(my_list)

    # Sort the list in descending order using the sort method
    my_list.sort(reverse=True)

    # Print the list after sorting
    print(my_list)
```

이 코드의 출력은 다음과 같다.

```
[5, 2, 7, 9, 1, 4, 6, 8, 3]
[9, 8, 7, 6, 5, 4, 3, 2, 1]
```

문자열 리스트를 정렬하려면 sort 메소드는 기본적으로 알파벳 순서를 사용한다. 예를 들어 다음 코드는 sort 메소드을 사용하여 문자열 리스트을 알파벳 순서로 정렬한다.

```python
# Create a list of strings
my_list = ["apple", "banana", "cherry", "date", "elderberry"]

# Print the list before sorting
print(my_list)

# Sort the list in alphabetical order using the sort method
my_list.sort()

# Print the list after sorting
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
['apple', 'banana', 'cherry', 'date', 'elderberry']
['apple', 'banana', 'cherry', 'date', 'elderberry']
```

sort 메소드에 `reverse=True` 인수를 전달하여 문자열 리스트을 알파벳 역 순으로 정렬할 수도 있다. 예를 들어, 다음 코드는 sort 메소드를 사용하여 문자열 리스트을 알파벳 역 순으로 정렬한다.

```python
# Create a list of strings
my_list = ["apple", "banana", "cherry", "date", "elderberry"]

# Print the list before sorting
print(my_list)

# Sort the list in reverse alphabetical order using the sort method
my_list.sort(reverse=True)

# Print the list after sorting
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
['apple', 'banana', 'cherry', 'date', 'elderberry']
['elderberry', 'date', 'cherry', 'banana', 'apple']
```

문자열의 리스트을 길이를 기준으로 정렬하고 싶다면, `key=len` 인수를 sort 메소드에 전달하면 된다.` key` 인수는 리스트의 각 요소에 대한 값을 반환하는 함수를 취하고, sort 메소드는 그 값을 사용하여 요소를 비교한다. 예를 들어, 다음 코드는 sort 메소드를 사용하여 문자열의 리스트을 길이를 기준으로 정렬한다.

```python
# Create a list of strings
my_list = ["apple", "banana", "cherry", "date", "elderberry"]

# Print the list before sorting
print(my_list)

# Sort the list based on their length using the sort method
my_list.sort(key=len)

# Print the list after sorting
print(my_list)
```

이 코드의 출력은 다음과 같다.

```
['apple', 'banana', 'cherry', 'date', 'elderberry']
['date', 'apple', 'banana', 'cherry', 'elderberry']
```

다음 절에서는 포스팅을 마무리하고 추가 리소스 몇 개를 제공하고자 한다.

## <a name="conclusion"></a> 맺으며
Python에서 리스트 메소드를 사용하는 방법에 대한 포스팅을 완료했으며 아래의 방법을 배웠다.

- Python에서 리스트 작성과 액세스
- append 메소드를 사용하여 리스트에 요소 추가
- pop 메소드를 사용하여 리스트에서 요소 제거
- sort 메소드를 사용하여 리스트 정렬

Python에서 리스트 메소드는 데이터를 조작하고 정리하는 데 매우 유용하다. 이들을 사용하여 요소를 추가, 제거, 정렬하는 등 리스트에 대한 다양한 작업을 수행할 수 있다. 삽입, 제거, 카운트, 인덱스, 리버스, 복사 등과 같은 다른 리스트 메소드를 사용하여 리스트에 대한 보다 고급 작업을 수행할 수 있다. 리스트 메소드에 대한 보다 자세한 내용은 [official Python Documentation](https://docs.python.org/3/tutorial/datastructures.html#more-on-lists)을 참조하세요. 
